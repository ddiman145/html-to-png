import {launch} from "puppeteer";
import {renderFile} from "ejs";
import {resolve} from 'node:path'

export async function renderTemplate(templateType, data) {
    const templateData = prepareTemplateData(data);

    const {templatePath, height, width} = getTemplate(templateType);

    const {browser, page} = await initPuppeteer(width, height);
    await page.setContent(
        await renderFile(
            resolve(templatePath),
            templateData
        )
    );

    const screenshot = await page.screenshot({type: 'png',});
    await browser.close();

    return screenshot;
}

function getTemplate(templateType) {
    switch (templateType) {
        case 'insta':
            return {templatePath: 'views/insta.ejs', width: 1080, height: 1920}
        case 'fb':
            return {templatePath: 'views/fb.ejs', width: 1080, height: 1080}
        default:
            throw new Error(`Unknown template type ${templateType}`)
    }
}

async function initPuppeteer(width, height) {
    const browser = await launch({args: ['--allow-file-access-from-files', '--enable-local-file-accesses'], headless: 'new'});
    const page = await browser.newPage();
    await page.setViewport({width: width, height: height});

    return {browser, page};
}

export function prepareTemplateData(data) {
    return {
        'logo': data['logo'] !== undefined ? {
            value: data['logo'],
            styles: prepareStyles('logo', data, true)
        } : null,
        'offerName': data['offerName'] !== undefined ? {
            value: data['offerName'],
            styles: prepareStyles('offer', data)
        } : null,
        'salary': data['salary'] !== undefined ? {
            value: data['salary'],
            styles: prepareStyles('salary', data)
        } : null,
        'pointsHeading': data['pointsHeading'] !== undefined ? {
            value: data['pointsHeading'],
            styles: prepareStyles('pointsHeading', data)
        } : null,
        'offerCollaborations': data['offerCollaborations'] !== undefined ? {
            value: data['offerCollaborations'],
            styles: prepareStyles('offerCollaborations', data)
        } : null,
        'points': preparePoints(data),
    };
}

function preparePoints(data) {
    const possibleKeys = ['pointOne', 'pointTwo', 'pointThree', 'pointFour', 'pointFive'];
    const points = [];

    for (const possibleKey of possibleKeys) {
        if (data[possibleKey] === undefined) {
            continue;
        }

        points.push({
            value: data[possibleKey],
            styles: prepareStyles(possibleKey, data)
        })
    }

    return points.length === 0 ? null : points;
}

function prepareStyles(key, data, isImg = false) {
    const maxWidthKey = isImg === true ? 'SizeX' : 'CharsToWrap';
    const styles = {
        'font-size': (data[`${key}Size`] !== undefined && data[`${key}Size`] !== '') ? (data[`${key}Size`] + 'px') : '',
        'margin-left': (data[`${key}PositionX`] !== undefined && data[`${key}PositionX`] !== '') ? (data[`${key}PositionX`] + 'px') : '',
        'top': (data[`${key}PositionY`] !== undefined && data[`${key}PositionY`] !== '') ? (data[`${key}PositionY`] + 'px') : '',
        'max-width': (data[`${key}${maxWidthKey}`] !== undefined && data[`${key}${maxWidthKey}`] !== '') ? (data[`${key}${maxWidthKey}`] + (isImg === true ? 'px' : 'ch')) : '',
        'max-height': (data[`${key}SizeY`] !== undefined && data[`${key}SizeY`] !== '') ? (data[`${key}SizeY`] + 'px') : '',
    }

    return Object.entries(styles)
        .filter(([key, value]) => value !== '')
        .map(([key, value]) => `${key}:${value}`)
        .join(";");
}
