import dotenv from 'dotenv';
export function loadEnvConfig() {
// Determine the environment based on NODE_ENV or any other variable
    const environment = process.env.NODE_ENV || 'local';
    console.log(process.env.NODE_ENV)
    switch (environment) {
        case 'prod':
            dotenv.config({ path: '.env.prod' });
            break;
        case 'dev':
            dotenv.config({ path: '.env.dev' });
            break;
        case 'local':
            dotenv.config({ path: '.env.local' });
            break;
    }
}
