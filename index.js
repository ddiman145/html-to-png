import express from 'express';
import {prepareTemplateData, renderTemplate} from './render.js';
import {loadEnvConfig} from './config.js';
import {resolve} from 'node:path';

loadEnvConfig();
// console.log(process.env);
const app = express();

app.use(
    express.static(resolve('public'))
);

app.listen(process.env.PORT, () => {
    console.log(`Server running on ${process.env.PORT}`);
})


app.get('/fb', async (req, res) => {
    try {
        res.set('Content-Type', 'image/png');
        const img = await renderTemplate('fb', req.query);
        res.send(img);
    } catch (error) {
        console.error('Error:', error);
        res.status(500).send('Internal Server Error');
    }
});

app.get('/insta', async (req, res) => {
    try {
        res.set('Content-Type', 'image/png');
        const img = await renderTemplate('insta', req.query);
        res.send(img);
    } catch (error) {
        console.error('Error:', error);
        res.status(500).send('Internal Server Error');
    }
});

app.get('/insta-test', (req, res) => {
    res.render('insta.ejs', prepareTemplateData(req.query));
});
